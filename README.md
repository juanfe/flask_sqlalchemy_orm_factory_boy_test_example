# Flask SQLAlchemy ORM test example by using pytest and factory_boy

Install the package SQLAlchemy Migrate:

```
$ pip install alembic factory_boy
```

The first time you must create a directory of a repository 'migrations' with
the versions:

```
$ rm migrations -rf
$ alembic init migrations
$ cd migrations
$ git checkout env.py
```

Create the database flask_test in postgres:

```
$ sudo -u postgres psql
postgres=# create database flask_test;
postgres=# grant all privileges on database flask_test to postgres;
```

Change in alembic.ini the database url, take care with the user and password
are the right:

```
sqlalchemy.url = postgresql+psycopg2://postgres:my_precious@localhost/flask_test
```

Now generate the first migrations and create the database:

```
alembic revision --autogenerate -m "initial"
```

This create the tables and the first migration in the dir migrations/versions.


To migrate until the most recent change use:

```
$ alembic upgrade head
```

To check the test use:

```
$ python
from example import *
from tests import commonses
commonses.Session.configure(bind=engine)
commonses.Session.query(Person).all()
from tests import examfactory
examfactory.PersonFactory()
```

To run the tests by using pytest:

```
$ cd tests
$ pytest
```
