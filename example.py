from flask import Flask

app = Flask(__name__)
app.config.from_pyfile('config.cfg')

from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

Base = declarative_base()


class Person(Base):
    __tablename__ = 'person'
    id = Column(Integer, primary_key=True)
    name = Column(String(29))
    pets = relationship('Pet', back_populates='owner')


class Pet(Base):
    __tablename__ = 'pet'
    id = Column(Integer, primary_key=True)
    name = Column(String(20))
    new = Column(String(20))
    owner_id = Column(Integer, ForeignKey('person.id'))
    owner = relationship("Person", back_populates='pets')


engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])

Base.metadata.create_all(engine)

if __name__ == '__main__':
    app.run()
