import factory
import factory.faker
import factory.fuzzy
from factory.alchemy import SQLAlchemyModelFactory

import os
import sys
MODEL_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
sys.path.append(MODEL_PATH)
import example

from .commonses import *


class PersonFactory(SQLAlchemyModelFactory):
    class Meta:
        model = example.Person
        sqlalchemy_session = Session

    id = factory.Sequence(lambda n: n)
    name = factory.faker.Faker('name')


class PetFactory(SQLAlchemyModelFactory):
    class Meta:
        model = example.Pet
        sqlalchemy_session = Session

    owner = factory.SubFactory(PersonFactory)
    name = factory.fuzzy.FuzzyText(length=10)
    new = factory.fuzzy.FuzzyText(length=10)
