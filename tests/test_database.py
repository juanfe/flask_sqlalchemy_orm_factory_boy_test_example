import sys
import os
import io
from configparser import ConfigParser
import sqlalchemy
#import pytest

from .commonses import *
from .examfactory import *
MODEL_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
sys.path.append(MODEL_PATH)
import example


def init():
    ini_str = '[root]\n' + open('../config.cfg', 'r').read()
    ini_fp = io.StringIO(ini_str)
    config = ConfigParser()
    config.read_file(ini_fp)
    engine = sqlalchemy.create_engine(config['root']['DATABASE_URL'][1:-1])

    Session.configure(bind=engine)


def test_Person():
    init()
    session = Session()
    p = PersonFactory()
    assert [p] == session.query(example.Person).all()
    session.rollback()
    Session.remove()
